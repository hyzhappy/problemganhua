# 2018刑侦科推理试题程序解

## 题目

1. 这道题的答案是
    
    `A.A B.B C.C D.D`

2. 第5题的答案是
    
    `A.C B.D C.A D.B`    

3. 以下选项中哪一题的答案与其他三项不同

    `A.第3题 B.第6题 C.第2题 D.第4题`

4. 以下选项中哪两题的答案相同

    `A.第1，5题 B.第2，7题 C.第1，9题 D.第6，10题`

5. 以下选项中哪一题的答案与本题相同

    `A.第8题 B.第4题 C.第9题 D.第7题`

6. 以下选项中哪两题的答案与第8题相同

    `A.第2，4题 B.第1，6题 C.第3，10题 D.第5，9题`
    
7. 在此十道题中，被选中次数最少的选项字母为

    `A.C B.B C.A D.D`

8. 以下哪一题的答案与第1题的答案在字母中不相邻

    `A.第7题 B.第5题 C.第2题 D.第10题`

9. 已知`第1题与第6题的答案相同`与`第X题与第5题的答案相同`的真假性相反，那么X为

    `A.第6题 B.第10题 C.第2题 D.第9题`

10. 在此10道题中，ABCD四个字母出现次数最多与最少者的差为

    `A.3 B.2 C.4 D.1`

# 程序解法

纯粹暴力搜索

# 人工解法思路

1. 从第三题入手，知道2346这四道题中有三道题的答案是一致的，另一道题与这三道题不一致。可以从第三题选B,C,D分别往下推理
2. 第7题的隐含意义是只有一个最少选项的字母，并且选的最少的次数是1
3. 同时根据第10题，可以排除第10题的D选项，第10题知道ABCD选项的次数从小到大可能的情况只有`1+2+2+5`，`1+2+3+4`，`1+3+3+3`这三种情况
