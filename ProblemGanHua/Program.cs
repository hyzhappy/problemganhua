﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemGanHua
{
    public abstract class Problem
    {
        protected List<Problem> _problems;

        public int Select
        {
            get; set;
        }

        public Problem(List<Problem> p)
        {
            _problems = p;
        }

        public void Display()
        {
            switch (Select)
            {
                case 0:
                    Console.Write("A");
                    break;
                case 1:
                    Console.Write("B");
                    break;
                case 2:
                    Console.Write("C");
                    break;
                case 3:
                    Console.Write("D");
                    break;
                default:
                    break;
            }
        }

        public abstract bool Judge();
    }

    public class Problem1 : Problem
    {
        public Problem1(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            return true;
        }
    }

    public class Problem2 : Problem
    {
        public Problem2(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            var p5 = _problems[4];
            switch (Select)
            {
                case 0:
                    return p5.Select == 2;
                case 1:
                    return p5.Select == 3;
                case 2:
                    return p5.Select == 0;
                case 3:
                    return p5.Select == 1;
            }
            return false;
        }
    }

    public class Problem3 : Problem
    {
        public Problem3(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            int i3 = _problems[2].Select;
            int i6 = _problems[5].Select;
            int i2 = _problems[1].Select;
            int i4 = _problems[3].Select;

            switch (Select)
            {
                case 0:
                    if ((i6 == i2) && (i6 == i4))
                    {
                        return i3 != i6;
                    }
                    return false;
                case 1:
                    if ((i3 == i2) && (i3 == i4))
                    {
                        return i3 != i6;
                    }
                    return false;
                case 2:
                    if ((i6 == i3) && (i6 == i4))
                    {
                        return i2 != i6;
                    }
                    return false;
                case 3:
                    if ((i6 == i2) && (i6 == i3))
                    {
                        return i4 != i6;
                    }
                    return false;
            }
            return false;
        }
    }

    public class Problem4 : Problem
    {
        public Problem4(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            switch (Select)
            {
                case 0:
                    return _problems[0].Select == _problems[4].Select;
                case 1:
                    return _problems[1].Select == _problems[6].Select;
                case 2:
                    return _problems[0].Select == _problems[8].Select;
                case 3:
                    return _problems[5].Select == _problems[9].Select;
            }
            return false;
        }
    }


    public class Problem5 : Problem
    {
        public Problem5(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            switch (Select)
            {
                case 0:
                    return _problems[7].Select == 0;
                case 1:
                    return _problems[3].Select == 1;
                case 2:
                    return _problems[8].Select == 2;
                case 3:
                    return _problems[6].Select == 3;
            }
            return false;
        }
    }

    public class Problem6 : Problem
    {
        public Problem6(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            var p8 = _problems[7];
            switch (Select)
            {
                case 0:
                    return (p8.Select == _problems[1].Select) && (p8.Select == _problems[3].Select);
                case 1:
                    return (p8.Select == _problems[0].Select) && (p8.Select == _problems[5].Select);
                case 2:
                    return (p8.Select == _problems[2].Select) && (p8.Select == _problems[9].Select);
                case 3:
                    return (p8.Select == _problems[4].Select) && (p8.Select == _problems[8].Select);
            }
            return false;
        }
    }

    public class Problem7 : Problem
    {
        public Problem7(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            int[] tmp = new int[4] { 0, 0, 0, 0 };
            foreach (var p in _problems)
            {
                tmp[p.Select]++;
            }

            int i = 0;
            int min = tmp[0];
            if (tmp[1] < min)
            {
                min = tmp[1];
                i = 1;
            }
            if (tmp[2] < min)
            {
                min = tmp[2];
                i = 2;
            }
            if (tmp[3] < min)
            {
                min = tmp[3];
                i = 3;
            }
            switch (Select)
            {
                case 0:
                    return i == 2;
                case 1:
                    return i == 1;
                case 2:
                    return i == 0;
                case 3:
                    return i == 3;
            }
            return false;
        }
    }

    class Problem8 : Problem
    {
        public Problem8(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            int i = _problems[0].Select;
            switch (Select)
            {
                case 0:
                    return Math.Abs(_problems[6].Select - i) > 1;
                case 1:
                    return Math.Abs(_problems[4].Select - i) > 1;
                case 2:
                    return Math.Abs(_problems[1].Select - i) > 1;
                case 3:
                    return Math.Abs(_problems[9].Select - i) > 1;
            }
            return false;
        }
    }

    public class Problem9 : Problem
    {
        public Problem9(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            var i5 = _problems[4].Select;
            bool b1 = _problems[0].Select == _problems[5].Select;
            bool b2 = false;
            switch (Select)
            {
                case 0:
                    b2 = i5 == _problems[5].Select;
                    break;
                case 1:
                    b2 = i5 == _problems[9].Select;
                    break;
                case 2:
                    b2 = i5 == _problems[1].Select;
                    break;
                case 3:
                    b2 = i5 == _problems[8].Select;
                    break;
            }
            return b1 ^ b2;
        }
    }

    public class Problem10 : Problem
    {
        public Problem10(List<Problem> p) : base(p)
        {
        }

        public override bool Judge()
        {
            int[] tmp = new int[] { 0, 0, 0, 0 };
            foreach (var p in _problems)
            {
                tmp[p.Select]++;
            }
            int max = tmp[0];
            int min = tmp[0];
            if (tmp[1] > max)
            {
                max = tmp[1];
            }
            if (tmp[1] < min)
            {
                min = tmp[1];
            }
            if (tmp[2] > max)
            {
                max = tmp[2];
            }
            if (tmp[2] < min)
            {
                min = tmp[2];
            }
            if (tmp[3] > max)
            {
                max = tmp[3];
            }
            if (tmp[3] < min)
            {
                min = tmp[3];
            }
            switch (Select)
            {
                case 0:
                    return max - min == 3;
                case 1:
                    return max - min == 2;
                case 2:
                    return max - min == 4;
                case 3:
                    return max - min == 1;
            }
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Problem> ps = new List<Problem>();
            Problem1 p1 = new Problem1(ps);
            Problem2 p2 = new Problem2(ps);
            Problem3 p3 = new Problem3(ps);
            Problem4 p4 = new Problem4(ps);
            Problem5 p5 = new Problem5(ps);
            Problem6 p6 = new Problem6(ps);
            Problem7 p7 = new Problem7(ps);
            Problem8 p8 = new Problem8(ps);
            Problem9 p9 = new Problem9(ps);
            Problem10 p10 = new Problem10(ps);
            ps.Add(p1);
            ps.Add(p2);
            ps.Add(p3);
            ps.Add(p4);
            ps.Add(p5);
            ps.Add(p6);
            ps.Add(p7);
            ps.Add(p8);
            ps.Add(p9);
            ps.Add(p10);
            int kk = 0;
            for (int i1 = 0; i1 < 4; i1++)
                for (int i2 = 0; i2 < 4; i2++)
                    for (int i3 = 0; i3 < 4; i3++)
                        for (int i4 = 0; i4 < 4; i4++)
                            for (int i5 = 0; i5 < 4; i5++)
                                for (int i6 = 0; i6 < 4; i6++)
                                    for (int i7 = 0; i7 < 4; i7++)
                                        for (int i8 = 0; i8 < 4; i8++)
                                            for (int i9 = 0; i9 < 4; i9++)
                                                for (int i10 = 0; i10 < 4; i10++)
                                                {
                                                    kk++;
                                                    p1.Select = i1;
                                                    p2.Select = i2;
                                                    p3.Select = i3;
                                                    p4.Select = i4;
                                                    p5.Select = i5;
                                                    p6.Select = i6;
                                                    p7.Select = i7;
                                                    p8.Select = i8;
                                                    p9.Select = i9;
                                                    p10.Select = i10;
                                                    bool b = true;
                                                    foreach (var p in ps)
                                                    {
                                                        b &= p.Judge();
                                                        if (!b)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    if (b)
                                                    {
                                                        Console.WriteLine("Search {0} times", kk);
                                                        Console.Write("Answer is: ");
                                                        foreach (var p in ps)
                                                        {
                                                            p.Display();
                                                        }
                                                        Console.WriteLine();
                                                        return;
                                                    }
                                                }
        }
    }
}
